/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 daimajia
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.daimajia.easing;

import com.daimajia.easing.back.BackEaseIn;
import com.daimajia.easing.back.BackEaseInOut;
import com.daimajia.easing.back.BackEaseOut;
import com.daimajia.easing.circ.CircEaseIn;
import com.daimajia.easing.circ.CircEaseInOut;
import com.daimajia.easing.circ.CircEaseOut;
import com.daimajia.easing.expo.ExpoEaseIn;
import com.daimajia.easing.expo.ExpoEaseInOut;
import com.daimajia.easing.expo.ExpoEaseOut;
import com.daimajia.easing.quad.QuadEaseIn;
import com.daimajia.easing.quad.QuadEaseInOut;
import com.daimajia.easing.quad.QuadEaseOut;
import com.daimajia.easing.sine.SineEaseInOut;
import com.daimajia.easing.sine.SineEaseOut;


public enum  Skill {

    BackEaseIn(BackEaseIn.class),
    BackEaseOut(BackEaseOut.class),
    BackEaseInOut(BackEaseInOut.class),

    BounceEaseIn(com.daimajia.easing.bounce.BounceEaseIn.class),
    BounceEaseOut(com.daimajia.easing.bounce.BounceEaseOut.class),
    BounceEaseInOut(com.daimajia.easing.bounce.BounceEaseInOut.class),

    CircEaseIn(CircEaseIn.class),
    CircEaseOut(CircEaseOut.class),
    CircEaseInOut(CircEaseInOut.class),

    CubicEaseIn(com.daimajia.easing.cubic.CubicEaseIn.class),
    CubicEaseOut(com.daimajia.easing.cubic.CubicEaseOut.class),
    CubicEaseInOut(com.daimajia.easing.cubic.CubicEaseInOut.class),

    ElasticEaseIn(com.daimajia.easing.elastic.ElasticEaseIn.class),
    ElasticEaseOut(com.daimajia.easing.elastic.ElasticEaseOut.class),

    ExpoEaseIn(ExpoEaseIn.class),
    ExpoEaseOut(ExpoEaseOut.class),
    ExpoEaseInOut(ExpoEaseInOut.class),

    QuadEaseIn(QuadEaseIn.class),
    QuadEaseOut(QuadEaseOut.class),
    QuadEaseInOut(QuadEaseInOut.class),

    QuintEaseIn(com.daimajia.easing.quint.QuintEaseIn.class),
    QuintEaseOut(com.daimajia.easing.quint.QuintEaseOut.class),
    QuintEaseInOut(com.daimajia.easing.quint.QuintEaseInOut.class),

    SineEaseIn(com.daimajia.easing.sine.SineEaseIn.class),
    SineEaseOut(SineEaseOut.class),
    SineEaseInOut(SineEaseInOut.class),

    Linear(com.daimajia.easing.linear.Linear.class);


    private Class easingMethod;

    private Skill(Class clazz) {
        easingMethod = clazz;
    }

    /**
     * 控制动画表达式
     *
     * @param duration 时长
     * @return BaseEasingMethod
     */
    public BaseEasingMethod getMethod(float duration) {
        try {
            return (BaseEasingMethod)easingMethod.getConstructor(float.class).newInstance(duration);
        } catch (Exception e) {
            throw new Error("Can not init easingMethod instance");
        }
    }
}
