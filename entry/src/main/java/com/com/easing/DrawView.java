/*
 *
 *  * The MIT License (MIT)
 *  *
 *  * Copyright (c) 2014 daimajia
 *  *
 *  * Permission is hereby granted, free of charge, to any person obtaining a copy
 *  * of this software and associated documentation files (the "Software"), to deal
 *  * in the Software without restriction, including without limitation the rights
 *  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  * copies of the Software, and to permit persons to whom the Software is
 *  * furnished to do so, subject to the following conditions:
 *  *
 *  * The above copyright notice and this permission notice shall be included in all
 *  * copies or substantial portions of the Software.
 *  *
 *  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *  * SOFTWARE.
 *
 */

package com.com.easing;

import ohos.agp.components.AttrHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;

import java.util.ArrayList;

public class DrawView extends Component implements Component.DrawTask{

    private Paint mBackgroundPaint = new Paint();
    private Paint mLinePaint = new Paint();
    private Path path = new Path();
    private boolean start = false;

    private ArrayList<Float> mHistory = new ArrayList<Float>();

    public DrawView(Context context) {
        super(context);
    }

    public DrawView(Context context, AttrSet attrSet) {
        super(context, attrSet);
        initPaint();
        //添加绘制任务
        addDrawTask(this::onDraw);
    }

    public DrawView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

    private void initPaint() {
        mBackgroundPaint.setColor(Color.WHITE);
        mLinePaint.setColor(Color.BLACK);
        mLinePaint.setAntiAlias(true);
        mLinePaint.setStrokeWidth((float) 3.0);
        mLinePaint.setStyle(Paint.Style.STROKE_STYLE);
    }

    public static float dipToPixels(Context context, float dipValue) {
        return  AttrHelper.vp2px(dipValue,context);
    }

    public void drawPoint(float time, float duration, float y){
        float p = time/duration;
        float x = p*getWidth();
        float z = getHeight() + y;
        if(!start){
            path.moveTo(x,z);
            start = true;
        }
        path.lineTo(x,z);
        invalidate();
    }

    public void clear(){
        path.reset();
        start = false;
        invalidate();
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        float l = 0;
        float t = getHeight() - getPaddingBottom() - dipToPixels(getContext(),217);
        float r = getWidth() - getPaddingRight();
        float b = getHeight() - dipToPixels(getContext(),60);
        RectFloat rect = new RectFloat(l,t,r,b);
        canvas.drawRect(rect,mLinePaint);
        canvas.drawPath(path,mLinePaint);
    }
}
