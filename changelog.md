## 2.4
  已支持部分：
  * BackEaseIn
  * BackEaseOut
  * BackEaseInOut
  * BounceEaseIn
  * BounceEaseOut
  * BounceEaseInOut
  * CircEaseIn
  * CircEaseOut
  * CircEaseInOut
  * CubicEaseIn
  * CubicEaseOut
  * CubicEaseInOut
  * ElasticEaseIn
  * ElasticEaseOut
  * ExpoEaseIn
  * ExpoEaseOut
  * ExpoEaseInOut
  * QuadEaseIn
  * QuadEaseOut
  * QuadEaseInOut
  * QuintEaseIn
  * QuintEaseOut
  * QuintEaseInOut
  * SineEaseIn
  * SineEaseOut
  * SineEaseInOut
  * Linear

  未支持部分：
  无
