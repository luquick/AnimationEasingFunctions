/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 daimajia
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.daimajia.easing;

import ohos.agp.animation.AnimatorValue;

public class Glider {

    /**
     * 设置动画
     *
     * @param skill   动画方式
     * @param duration 时长
     * @param animator   数值动画
     * @param valueUpdateListener   动画监听器
     * @return 动画对象
     */
    public static AnimatorValue glide(
            Skill skill,
            float duration,
            AnimatorValue animator,
            AnimatorValue.ValueUpdateListener valueUpdateListener) {
        animator.setValueUpdateListener(valueUpdateListener);
        return animator;
    }

    public static AnimatorValue glide(Skill skill, float duration, AnimatorValue animator) {
        BaseEasingMethod t = skill.getMethod(duration);
        return animator;
    }
}
