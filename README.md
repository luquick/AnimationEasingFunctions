# AnimationEasingFunctions

## 目录
* [功能介绍](#功能介绍)
* [演示](#演示)
* [集成](#集成)
* [使用示例](#使用示例)
* [接口说明](#接口说明)


## 功能介绍

- [x] ohos动画缓动功能。让动画更逼真！
  实现下列多种动画运动轨迹
- BackEaseIn
- BackEaseOut
- BackEaseInOut
- BounceEaseIn
- BounceEaseOut
- BounceEaseInOut
- CircEaseIn
- CircEaseOut
- CircEaseInOut
- CubicEaseIn
- CubicEaseOut
- CubicEaseInOut
- ElasticEaseIn
- ElasticEaseOut
- ExpoEaseIn
- ExpoEaseOut
- ExpoEaseInOut
- QuadEaseIn
- QuadEaseOut
- QuadEaseInOut
- QuintEaseIn
- QuintEaseOut
- QuintEaseInOut
- SineEaseIn
- SineEaseOut
- SineEaseInOut
- Linear

## 演示

|缓动动画 | 缓动动画|
|:---:|:---:|
|<img src="screenshot/AnimationOne.gif" width="75%"/>|<img src="screenshot/AnimationTwo.gif" width="75%"/>|



## 集成

```
方式一：
将项目中的build.gradle文件中dependencies→classpath版本改为对应的版本（即你的IDE新建项目中所用的版本）
添加har到entry下的libs文件夹内
entry的build.gradle内添加如下代码
implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])


方式二：
allprojects{
    repositories{
        mavenCentral()
    }
}
implementation 'io.openharmony.tpc.thirdlib:AnimationEasingFunctions:1.0.2' 

```


## 使用示例


```
    private ListContainer mEasingList;
    private EasingAdapter mAdapter;
    private Component mTarget;
    private long mDuration = 2000;
    private DrawView mHistory;

        mEasingList = (ListContainer) findComponentById(ResourceTable.Id_easing_list);
        mAdapter = new EasingAdapter(this);
        mEasingList.setItemProvider(mAdapter);
        mTarget = findComponentById(ResourceTable.Id_target);
        mHistory = (DrawView) findComponentById(ResourceTable.Id_history);
        mEasingList.setItemClickedListener(new ListContainer.ItemClickedListener() {
            @Override
            public void onItemClicked(ListContainer listContainer, Component component, int position, long var4) {
                mHistory.clear();
                Skill skill = (Skill) component.getTag();
                // 动画集合
                AnimatorGroup set = new AnimatorGroup();
                mTarget.setTranslationX(0);
                mTarget.setTranslationY(0);
                // 数值动画
                AnimatorValue animator = new AnimatorValue();
                animator.setDuration(mDuration);
                BaseEasingMethod easing = skill.getMethod(mDuration);
                // 画最开始的点
                mHistory.drawPoint(0, mDuration, 0 - vpToPixels(getContext(), 60));
                set.runSerially(
                        Glider.glide(skill, mDuration, animator,new AnimatorValue.ValueUpdateListener() {
                    @Override
                    public void onUpdate(AnimatorValue animatorValue, float value) {

                        float start = 0;
                        float end = -vpToPixels(getContext(), 157) ;
                        float result = easing.evaluate(value, start, end);
                        mTarget.setTranslationY(result);
                        mHistory.drawPoint(mDuration * value, mDuration, result - vpToPixels(getContext(), 60));
                    }
                }));
                set.setDuration(mDuration);
                set.start();
            }
        });

```

## 接口说明


```java

     /**
     * 设置动画
     *
     * @param skill   动画方式
     * @param duration 时长
     * @param animator   数值动画
     * @param valueUpdateListener   动画监听器
     * @return 动画对象
     */
    public static AnimatorValue glide(
            Skill skill,
            float duration,
            AnimatorValue animator,
            AnimatorValue.ValueUpdateListener valueUpdateListener)

     /**
     * 设置动画
     *
     * @param skill   动画方式
     * @param duration 时长
     * @param animator   数值动画
     * @return 动画对象
     */
    public static AnimatorValue glide(
            Skill skill,
            float duration,
            AnimatorValue animator)

      /**
      * 控制动画表达式
      *
      * @param duration 时长
      * @return BaseEasingMethod
      */
     public BaseEasingMethod getMethod(float duration)

    /**
     * 获得动画结果
     *
     * @param fraction 动画完成度
     * @param startValue 动画开始位置
     * @param endValue 动画结束位置
     * @return 动画结果
     */
    public final Float evaluate(float fraction, Float startValue, Float endValue)

```

