package com.com.easing;



import com.daimajia.easing.BaseEasingMethod;
import com.daimajia.easing.Skill;

import ohos.agp.components.*;
import ohos.app.Context;

public class EasingAdapter extends BaseItemProvider {

    private Context mContext;
    public EasingAdapter(Context context) {
        mContext = context;
    }

    @Override
    public int getCount() {
        return Skill.values().length;
    }

    @Override
    public Object getItem(int i) {
        return Skill.values()[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        Object o = getItem(i);
        BaseEasingMethod b = ((Skill)o).getMethod(1000);
        int start = b.getClass().getName().lastIndexOf(".") + 1;
        String name = b.getClass().getName().substring(start);
        Component v = LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_item,componentContainer,false);

        Text tv = (Text)v.findComponentById(ResourceTable.Id_list_item_text);
        tv.setText(name);
        v.setTag(o);
        return v;
    }
}
